# rtl8723ds

1.原来的 Makefile 配置是为 386 PC 的。为 stm32mp 添加了 CONFIG_PLATFORM_ARM_STM32MP = y 项

2.将 RTL871X 变量改名为 RTL8XXX

3.为 CONFIG_PLATFORM_ARM_STM32MP 选项 ARCH 、工具链和目录项， kernel 目录项根据实际情况修改
ifeq ($(CONFIG_PLATFORM_ARM_STM32MP), y)
EXTRA_CFLAGS += -DCONFIG_LITTLE_ENDIAN
ARCH := arm
CROSS_COMPILE := $(ARCH)-ostl-linux-gnueabi-
KVER  := 5.10.61
KSRC ?= $(PWD)/../build-5.10
endif

4.struct sha256_state 结构与 linux 的同名结构冲突，导致编译不通过，将其重命名为 struct rtl_sha256_state 。



```
git clone https://gitee.com/wslmc/rtl8723ds.git
cd rtl8723ds
make
sudo make install
sudo modprobe -v 8723ds

